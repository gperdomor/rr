include:
  - template: Workflows/MergeRequest-Pipelines.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Jobs/Code-Quality.gitlab-ci.yml
  - template: Jobs/Code-Intelligence.gitlab-ci.yml

stages:
  - setup
  - test
  - docker

default:
  image: node:18.0.0-alpine

variables:
  CYPRESS_INSTALL_BINARY: 0
  GIT_DEPTH: 0
  NEXT_TELEMETRY_DISABLED: 1
  NX_CLOUD_ACCESS_TOKEN: $NX_CLOUD_AUTH_TOKEN
  NX_CLOUD_DISTRIBUTED_EXECUTION: 'true'
  NX_RUN_GROUP: $CI_PIPELINE_ID

###############################
### Included Jobs

license_scanning:
  needs: []
  variables:
    ASDF_NODEJS_VERSION: '18.0'

# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
secret_detection:
  needs: []

# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
dependency_scanning:
  needs: []

# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
sast:
  needs: []

code_quality:
  artifacts:
    paths: [gl-code-quality-report.json]

###############################
### Templates

.node-job:
  cache:
    key:
      prefix: $CI_PROJECT_NAME
      files:
        - package-lock.json
    paths:
      - .npm/
    policy: pull
  before_script:
    - npm ci --cache .npm --prefer-offline

.require-install-deps:
  needs:
    - job: install-dependencies
      optional: true

.nx-set-sha:
  before_script:
    # Getting latest success pipeline on main branch
    - npx nx-set-shas gitlab -t $API_TOKEN -o nx.env
    - set -a
      [ -f nx.env ] && . nx.env
      set +a

###############################

install-dependencies:
  stage: setup
  interruptible: true
  extends: [.node-job]
  cache:
    key: !reference [.node-job, cache, key]
    paths: !reference [.node-job, cache, paths]
    policy: pull-push
  script:
    - echo "Done!..."
  rules:
    - changes:
        - package-lock.json

agents:
  stage: test
  extends: [.node-job, .require-install-deps]
  parallel: 2
  variables:
    POSTGRES_DB: rollouts
    POSTGRES_USER: roll
    POSTGRES_PASSWORD: outs
    DATABASE_URL: postgres://roll:outs@database/rollouts
  services:
    - name: postgres:14.1-alpine
      alias: database
  before_script:
    - !reference [.node-job, before_script]
    - !reference [.nx-set-sha, before_script]
  script:
    - echo "Start Nx Agent $CI_NODE_INDEX of $CI_NODE_TOTAL"
    - apk add --no-cache git
    - NX_CLOUD_DISTRIBUTED_EXECUTION=false npx nx affected --target=prisma-generate --parallel
    - npx nx-cloud start-agent

main:
  extends: [.node-job, .require-install-deps]
  stage: test
  variables:
    NX_BRANCH: $CI_COMMIT_BRANCH
  before_script:
    - apk add --no-cache git
    - !reference [.node-job, before_script]
    - !reference [.nx-set-sha, before_script]
  script:
    - npx nx-cloud start-ci-run
    - NX_CLOUD_DISTRIBUTED_EXECUTION=false npx nx workspace-lint
    - NX_CLOUD_DISTRIBUTED_EXECUTION=false npx nx format:check
    - npx nx affected --target=build --parallel --max-parallel=3 &
    - npx nx affected --target=lint  --parallel --max-parallel=3 &
    - npx nx affected --target=test  --parallel --max-parallel=2 --code-coverage &
    # - npx nx affected --target=e2e   --parallel --max-parallel=2 &
    - wait
  after_script:
    - npx nx-cloud stop-all-agents
    - sleep 5
    - echo 'Done!'
  coverage: '/All files[^|]*\|[^|]*\s+([\d\.]+)/'
  artifacts:
    paths:
      - dist/
    reports:
      dotenv: nx.env
      coverage_report:
        coverage_format: cobertura
        path: coverage/**/cobertura-coverage.xml
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: delayed
      start_in: 20 seconds

mr:
  extends: [main]
  variables:
    NX_BRANCH: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: delayed
      start_in: 20 seconds

main:docker:
  extends: [.node-job, .require-install-deps]
  stage: docker
  image: gperdomor/nx-docker:18.0.0-alpine
  services:
    - docker:20.10.15-dind
  needs:
    - job: main
  variables:
    GIT_DEPTH: 0
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
    INPUT_PUSH: 'true'
    # API
    INPUT_API_IMAGES: registry.gitlab.com/gperdomor/rr/api,registry.heroku.com/rollouts-api/web
    # STUDIO
    INPUT_STUDIO_IMAGES: registry.gitlab.com/gperdomor/rr/studio,registry.heroku.com/rollouts-studio/web
  before_script:
    - apk add --no-cache git
    - !reference [.node-job, before_script]
    - !reference [.nx-set-sha, before_script]
  script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - docker buildx create --use
    - npx nx affected --target=docker
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
