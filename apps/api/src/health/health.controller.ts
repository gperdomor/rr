import { Controller, Get } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { DiskHealthIndicator, HealthCheck, HealthCheckService, MemoryHealthIndicator } from '@nestjs/terminus';
import { PrismaHealthIndicator } from '../prisma/prisma.health';
import { Public } from '../public.decorator';

@ApiTags('Health Checks')
@Controller('_/healthz')
export class HealthController {
  constructor(
    private readonly health: HealthCheckService,
    private readonly memory: MemoryHealthIndicator,
    private readonly disk: DiskHealthIndicator,
    private readonly prisma: PrismaHealthIndicator
  ) {}

  @Public()
  @Get('liveness')
  @ApiOkResponse({
    description: 'The application is alive',
    schema: {
      type: 'object',
      properties: {
        status: {
          type: 'string',
          example: 'ok',
        },
      },
    },
  })
  async liveness(): Promise<{ status: 'ok' }> {
    return { status: 'ok' };
  }

  @Public()
  @Get('readiness')
  @HealthCheck()
  async check() {
    return this.health.check([
      () => this.prisma.pingCheck('database'),
      () => this.disk.checkStorage('storage', { thresholdPercent: 0.9, path: '/' }),
      () => this.memory.checkHeap('memory_heap', 250 * 1024 * 1024),
      () => this.memory.checkRSS('memory_rss', 250 * 1024 * 1024),
    ]);
  }
}
