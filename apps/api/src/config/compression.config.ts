import { registerAs } from '@nestjs/config';
import { FastifyCompressOptions } from 'fastify-compress';
import * as Joi from 'joi';

interface CompressionConfig {
  enabled: boolean;
  options: FastifyCompressOptions;
}

export const compressionConfig = registerAs<CompressionConfig>('compression', () => ({
  enabled: process.env.COMPRESSION_ENABLED === 'true',
  options: {
    threshold: Number(process.env.COMPRESSION_THRESHOLD),
    encodings: process.env.COMPRESSION_ENCODINGS.split(',').map((encoding) =>
      encoding.trim()
    ) as FastifyCompressOptions['encodings'],
  },
}));

const MIN_THRESHOLD = 1024;

export const compressionConfigSchema = Joi.object({
  COMPRESSION: Joi.boolean().default(true),
  COMPRESSION_THRESHOLD: Joi.number().min(MIN_THRESHOLD).default(MIN_THRESHOLD),
  COMPRESSION_ENCODINGS: Joi.string().default('br, gzip, deflate, identity'),
});
