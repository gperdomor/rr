import { registerAs } from '@nestjs/config';
import * as Joi from 'joi';

interface AuthConfig {
  jwtSecret: string;
}

export const authConfig = registerAs<AuthConfig>('auth', () => ({
  jwtSecret: process.env.JWT_SECRET,
}));

export const authConfigSchema = Joi.object({
  JWT_SECRET: Joi.string().min(20).required(),
});
