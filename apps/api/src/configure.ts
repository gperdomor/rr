import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { NestFastifyApplication } from '@nestjs/platform-fastify';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import compression from 'fastify-compress';
import { fastifyHelmet as helmet } from 'fastify-helmet';
import { compressionConfig } from './config/compression.config';
import { oasConfig } from './config/oas.config';
import { corsOptions, helmetOptions, validationPipeOptions } from './constants';
import { PrismaService } from './prisma/prisma.service';

export const configure = async (app: NestFastifyApplication) => {
  app.enableCors(corsOptions);

  await app.register(helmet, helmetOptions);

  await configureCompression(app);

  app.useGlobalPipes(new ValidationPipe(validationPipeOptions));

  await configureOAS(app);

  app.enableShutdownHooks();

  /**
   * Prisma interferes with NestJS enableShutdownHooks. Prisma listens for shutdown signals and will
   * call process.exit() before your application shutdown hooks fire. To deal with this, you would
   * need to add a listener for Prisma beforeExit event.
   */
  const prismaService: PrismaService = app.get(PrismaService);
  prismaService.enableShutdownHooks(app);
};

const configureCompression = async (app: NestFastifyApplication) => {
  const compressionConf: ConfigType<typeof compressionConfig> = app.get(compressionConfig.KEY);

  if (compressionConf.enabled) {
    await app.register(compression, compressionConf.options);
    Logger.log(`🗜 Compression is enabled for encodings: ${compressionConf.options.encodings.join(', ')}`, 'Configure');
  } else {
    Logger.log(`🗜 Compression is disabled`, 'Configure');
  }
};

const configureOAS = async (app: NestFastifyApplication) => {
  const oasConf: ConfigType<typeof oasConfig> = app.get(oasConfig.KEY);

  if (oasConf.enabled) {
    const config = new DocumentBuilder()
      .setTitle('Flags API')
      .setDescription('The feature flags API description')
      .setVersion('1.0')
      .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup(oasConf.path, app, document);

    Logger.log(`📖 Open API Specification is enabled under /${oasConf.path} path`, 'Configure');
  } else {
    Logger.log(`📖 Open API Specification is disabled`, 'Configure');
  }
};
