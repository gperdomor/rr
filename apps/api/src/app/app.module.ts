import { Logger, Module, OnApplicationShutdown } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { AuthModule } from '../auth/auth.module';
import { appConfig, appConfigSchema } from '../config/app.config';
import { authConfig, authConfigSchema } from '../config/auth.config';
import { compressionConfig, compressionConfigSchema } from '../config/compression.config';
import { databaseConfigSchema } from '../config/database.config';
import { oasConfig, oasConfigSchema } from '../config/oas.config';
import { HealthModule } from '../health/health.module';
import { PrismaModule } from '../prisma/prisma.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      cache: true,
      expandVariables: true,
      load: [appConfig, oasConfig, compressionConfig, authConfig],
      validationSchema: Joi.object()
        .concat(appConfigSchema)
        .concat(oasConfigSchema)
        .concat(compressionConfigSchema)
        .concat(authConfigSchema)
        .concat(databaseConfigSchema),
    }),
    AuthModule,
    HealthModule,
    PrismaModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements OnApplicationShutdown {
  private readonly logger = new Logger(AppModule.name);

  onApplicationShutdown(signal: string) {
    this.logger.log(`🛑 ${signal} received, exiting now...`);
  }
}
