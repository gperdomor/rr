import { Controller, Get } from '@nestjs/common';
import { User } from '../auth/user.decorator';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getData() {
    return this.appService.getData();
  }

  @Get('me')
  getUser(@User() user): Record<string, unknown> {
    return user;
  }
}
