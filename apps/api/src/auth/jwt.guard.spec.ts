import { Reflector } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthModule } from './auth.module';
import { AuthService } from './auth.service';
import { JwtGuard } from './jwt.guard';

describe('JwtGuard', () => {
  let guard: JwtGuard;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [JwtModule.register({}), AuthModule],
      providers: [AuthService],
    }).compile();

    const reflector = module.get<Reflector>(Reflector);
    const authService = module.get<AuthService>(AuthService);

    guard = new JwtGuard(reflector, authService);
  });

  it('should be defined', () => {
    expect(guard).toBeDefined();
  });
});
