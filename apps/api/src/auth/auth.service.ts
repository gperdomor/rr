import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(private readonly jwt: JwtService) {}

  validate(payload: string): Record<string, any> {
    return this.jwt.verify(payload);
  }
}
