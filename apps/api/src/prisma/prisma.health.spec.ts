import { HealthCheckError } from '@nestjs/terminus';
import { Test, TestingModule } from '@nestjs/testing';
import { PrismaHealthIndicator } from './prisma.health';
import { PrismaService } from './prisma.service';

describe('PrismaHealthIndicatorService', () => {
  let healthService: PrismaHealthIndicator;
  let prismaService: PrismaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PrismaHealthIndicator,
        {
          provide: PrismaService,
          useValue: {
            $queryRaw: jest.fn(),
          },
        },
      ],
    }).compile();

    healthService = module.get<PrismaHealthIndicator>(PrismaHealthIndicator);
    prismaService = module.get<PrismaService>(PrismaService);
  });

  it('should be defined', () => {
    expect(healthService).toBeDefined();
  });

  it('should return success', async () => {
    jest.spyOn(prismaService, '$queryRaw').mockResolvedValue([1]);

    const result = await healthService.pingCheck('database');

    expect(result).toMatchObject({
      database: {
        status: 'up',
      },
    });
  });

  it('should throws error if database query fails', async () => {
    jest.spyOn(prismaService, '$queryRaw').mockImplementation(() => {
      throw new Error();
    });

    expect.assertions(2);

    try {
      await healthService.pingCheck('database');
    } catch (e) {
      expect(e).toBeInstanceOf(HealthCheckError);
      expect(e.message).toMatch('Database check failed');
    }
  });
});
