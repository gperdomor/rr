// This is your Prisma schema file,
// learn more about it in the docs: https://pris.ly/d/prisma-schema

generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

model Addons {
  id          String    @id @default(cuid()) @db.VarChar(100)
  provider    String
  description String?
  enabled     Boolean?  @default(true)
  parameters  Json?
  events      Json?
  createdAt   DateTime? @default(now()) @map("created_at")
  updatedAt   DateTime? @default(now()) @map("updated_at")

  @@map("addons")
}

model ApiTokens {
  secret          String        @id
  username        String
  type            String
  createdAt       DateTime      @default(now()) @map("created_at")
  expiresAt       DateTime?     @map("expires_at")
  seenAt          DateTime?     @map("seen_at")
  projectId       String?       @map("project_id") @db.VarChar(100)
  environmentName String?       @map("environment_name") @db.VarChar(100)
  environments    Environments? @relation(fields: [environmentName], references: [name], onDelete: Cascade, onUpdate: NoAction)
  projects        Projects?     @relation(fields: [projectId], references: [id], onDelete: Cascade, onUpdate: NoAction)

  @@map("api_tokens")
}

model Environments {
  name                String                @id @db.VarChar(100)
  type                String
  enabled             Boolean?              @default(true)
  sortOrder           Int?                  @default(9999) @map("sort_order")
  protected           Boolean?              @default(false)
  createdAt           DateTime              @default(now()) @map("created_at")
  updatedAt           DateTime?             @default(now()) @map("updated_at")
  apiTokens           ApiTokens[]
  featureEnvironments FeatureEnvironments[]
  featureStrategies   FeatureStrategies[]
  projectEnvironments ProjectEnvironments[]

  @@map("environments")
}

model FeatureEnvironments {
  environmentName String       @default("default") @map("environment_name") @db.VarChar(100)
  featureName     String       @map("feature_name") @db.VarChar(100)
  enabled         Boolean
  environments    Environments @relation(fields: [environmentName], references: [name], onDelete: Cascade, onUpdate: NoAction)
  features        Features     @relation(fields: [featureName], references: [name], onDelete: Cascade, onUpdate: NoAction)

  @@id([environmentName, featureName])
  @@index([featureName])
  @@map("feature_environments")
}

model FeatureStrategies {
  id              String       @id
  featureName     String       @map("feature_name") @db.VarChar(100)
  projectName     String       @map("project_name") @db.VarChar(100)
  environmentName String       @default("default") @map("environment_name") @db.VarChar(100)
  strategyName    String       @map("strategy_name") @db.VarChar(100)
  parameters      Json         @default("{}")
  constraints     Json?
  sortOrder       Int          @default(9999) @map("sort_order")
  createdAt       DateTime?    @default(now()) @map("created_at")
  environments    Environments @relation(fields: [environmentName], references: [name], onDelete: Cascade, onUpdate: NoAction)
  features        Features     @relation(fields: [featureName], references: [name], onDelete: Cascade, onUpdate: NoAction)

  @@index([environmentName])
  @@index([featureName])
  @@map("feature_strategies")
}

model FeatureTag {
  featureName String   @map("feature_name") @db.VarChar(100)
  tagType     String   @map("tag_type")
  tagValue    String   @map("tag_value")
  createdAt   DateTime @default(now()) @map("created_at")
  features    Features @relation(fields: [featureName], references: [name], onDelete: Cascade, onUpdate: NoAction)
  tags        Tags     @relation(fields: [tagType, tagValue], references: [type, value], onDelete: Cascade, onUpdate: NoAction)

  @@id([featureName, tagType, tagValue])
  @@index([tagType, tagValue])
  @@map("feature_tag")
}

model FeatureTypes {
  id           String    @id @db.VarChar(100)
  name         String    @db.VarChar(100)
  description  String?
  lifetimeDays Int?      @map("lifetime_days")
  createdAt    DateTime? @default(now()) @map("created_at")

  @@map("feature_types")
}

model Features {
  name                String                @id @db.VarChar(100)
  description         String?
  archived            Boolean?              @default(false)
  variants            Json?                 @default("[]")
  type                String?               @default("release") @db.VarChar(100)
  stale               Boolean?              @default(false)
  project             String?               @default("default") @db.VarChar(100)
  createdAt           DateTime              @default(now()) @map("created_at")
  updatedAt           DateTime?             @map("updated_at")
  lastSeenAt          DateTime?             @map("last_seen_at")
  featureEnvironments FeatureEnvironments[]
  featureStrategies   FeatureStrategies[]
  featureTag          FeatureTag[]

  @@map("features")
}

model ProjectEnvironments {
  projectId       String       @map("project_id") @db.VarChar(100)
  environmentName String       @map("environment_name") @db.VarChar(100)
  environments    Environments @relation(fields: [environmentName], references: [name], onDelete: Cascade, onUpdate: NoAction)
  projects        Projects     @relation(fields: [projectId], references: [id], onDelete: Cascade, onUpdate: NoAction)

  @@id([projectId, environmentName])
  @@index([environmentName])
  @@map("project_environments")
}

model Projects {
  id           String                @id @default(cuid()) @db.VarChar(100)
  name         String                @db.VarChar(100)
  description  String?
  createdAt    DateTime?             @default(now()) @map("created_at")
  updatedAt    DateTime?             @default(now()) @map("updated_at")
  health       Int?                  @default(100)
  apiTokens    ApiTokens[]
  environments ProjectEnvironments[]

  @@map("projects")
}

model Settings {
  name    String @id @db.VarChar(255)
  content Json?

  @@map("settings")
}

model Strategies {
  name        String    @id @db.VarChar(100)
  displayName String?   @map("display_name")
  description String?
  parameters  Json?
  builtIn     Int?      @default(0) @map("built_in")
  deprecated  Boolean?  @default(false)
  sortOrder   Int?      @default(9999) @map("sort_order")
  createdAt   DateTime? @default(now()) @map("created_at")
  updatedAt   DateTime? @default(now()) @map("updated_at")

  @@map("strategies")
}

model TagTypes {
  name        String    @id
  description String?
  icon        String?
  createdAt   DateTime? @default(now()) @map("created_at")
  tags        Tags[]

  @@map("tag_types")
}

model Tags {
  type       String
  value      String
  createdAt  DateTime?    @default(now()) @map("created_at")
  tagTypes   TagTypes     @relation(fields: [type], references: [name], onDelete: Cascade, onUpdate: NoAction)
  featureTag FeatureTag[]

  @@id([type, value])
  @@map("tags")
}
