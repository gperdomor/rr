import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { databaseConfig } from '../config/database.config';
import { PrismaHealthIndicator } from './prisma.health';
import { PrismaService } from './prisma.service';

@Module({
  imports: [ConfigModule.forFeature(databaseConfig)],
  providers: [PrismaService, PrismaHealthIndicator],
  exports: [PrismaService, PrismaHealthIndicator],
})
export class PrismaModule {}
