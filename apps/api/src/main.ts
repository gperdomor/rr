import { Logger } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import * as hyperid from 'hyperid';
import { AppModule } from './app/app.module';
import { appConfig } from './config/app.config';
import { configure } from './configure';
import { SERVICE_NAME } from './constants';

const instance = hyperid();

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter({ genReqId: () => instance() }),
    { bufferLogs: true }
  );

  Logger.log(`Environment: ${process.env.NODE_ENV?.toUpperCase()}`, 'Bootstrap');

  const { host, port }: ConfigType<typeof appConfig> = app.get(appConfig.KEY);

  await configure(app);

  await app.listen(port, host);

  if (process.env.NODE_ENV === 'production') {
    Logger.log(`🚀 Application ${SERVICE_NAME} is running on port: ${port}`, 'Bootstrap');
  } else {
    Logger.log(`🚀 Application ${SERVICE_NAME} is running on: ${await app.getUrl()}`, 'Bootstrap');
  }
}

bootstrap();
